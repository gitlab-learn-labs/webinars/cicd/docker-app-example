FROM nginx

COPY wrapper.sh /

COPY public /usr/share/nginx/public

CMD ["./wrapper.sh"]
